#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This file is part of health_cepsap module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from setuptools import setup
import re
import os
import io
try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser

MODULE2PREFIX = {}


def read(fname):
    return io.open(
        os.path.join(os.path.dirname(__file__), fname),
        'r', encoding='utf-8').read()


def get_require_version(name):
    require = '%s >= %s.%s, < %s.%s'
    require %= (name, major_version, minor_version,
        major_version, minor_version + 1)
    return require


def get_health_require_version(name):
    require = '%s >= %s.%s, < %s.%s'
    require %= (name, health_major_version, health_minor_version,
        health_major_version, health_minor_version + 1)
    return require


config = ConfigParser()
config.readfp(open('tryton.cfg'))
info = dict(config.items('tryton'))
for key in ('depends', 'extras_depend', 'xml'):
    if key in info:
        info[key] = info[key].strip().splitlines()

version = info.get('version', '0.0.1')
major_version, minor_version, _ = version.split('.', 2)
major_version = int(major_version)
minor_version = int(minor_version)

health_version = dict(config.items('health')).get('version', '0.0.1')
health_major_version, health_minor_version, _ = health_version.split('.', 2)
health_major_version = int(health_major_version)
health_minor_version = int(health_minor_version)

name = 'health_cepsap'
download_url = 'http://www.silix.com.ar'

requires = []
for dep in info.get('depends', []):
    if dep.startswith('health'):
        prefix = MODULE2PREFIX.get(dep, 'trytond')
        requires.append(get_health_require_version('%s_%s' % (prefix, dep)))
    elif not re.match(r'(ir|res|webdav)(\W|$)', dep):
        prefix = MODULE2PREFIX.get(dep, 'trytond')
        requires.append(get_require_version('%s_%s' % (prefix, dep)))
requires.append(get_require_version('trytond'))

tests_require = [get_require_version('proteus')]
dependency_links = []

setup(name=name,
    version=version,
    description=('Módulo GNU Health para la CEPS-AP'),
    long_description=read('README'),
    author='Silix',
    author_email='contacto@silix.com.ar',
    url='http://www.silix.com.ar',
    download_url=download_url,
    package_dir={'trytond.modules.health_cepsap': '.'},
    packages=[
        'trytond.modules.health_cepsap',
        'trytond.modules.health_cepsap.tests',
        ],
    package_data={
        'trytond.modules.health_cepsap': (info.get('xml', []) + [
            'tryton.cfg', 'locale/*.po']),
        },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Plugins',
        'Framework :: Tryton',
        'Intended Audience :: Developers',
        'Intended Audience :: Healthcare Industry',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Natural Language :: English',
        'Natural Language :: Spanish',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Topic :: Scientific/Engineering :: Medical Science Apps.',
        ],
    license='GPL-3',
    install_requires=requires,
    dependency_links=dependency_links,
    zip_safe=False,
    entry_points="""
    [trytond.modules]
    health_cepsap = trytond.modules.health_cepsap
    """,
    test_suite='tests',
    test_loader='trytond.test_loader:Loader',
    tests_require=tests_require,
    use_2to3=True,
    )
